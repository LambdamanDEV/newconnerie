<?php

class Vehicule
{

    protected $masse;
    protected $vitesse;
    protected $dimension;
    protected $rayonCourbure;

    public function calculEnergieCinetique(){
        if($this->masse>0){
            return 0.5 * this->masse * abs(this->vitesse**2);
        }else
            return false;
    }

    public function getMasse(){
        return $this->masse;
    }
    public function setMasse(float $n): float{
        $this->masse = $n;
    }

    public function getVitesse(){
        return $this->vitesse;
    }
    public function setVitesse($n): float{
        $this->vitesse = $n;
    }

    public function forceCentrifuge($masse){
        if($this->rayonCourbure !== 0){
            return self::calculEnergieCinetique()/$this->rayonCourbure;
        }else
            return false;
    }
}