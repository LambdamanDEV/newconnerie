<?php

class Voiture extends Vehicule
{
public $mass;
public $color = 'Blanche';
public $nbrRapport;
public $nbrPlace;

    public function _construct($place){
        $this->nbrPlace = $place;
    }

    public function renvoyerColor(){
        return $this->color;
    }

    public function _destruct(){
        echo 'inscrit :';
    }
}